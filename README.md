# Bad Apple Northstar Masterserver

A [Northstar](https://northstar.tf/) Masterserver that plays [Bad Apple](https://www.youtube.com/watch?v=UkgK8eUdpAo) when loading the server browser list in-game.

## Demo (YouTube link)

[![YouTube link](https://i.ytimg.com/vi_webp/KxwkkXk_wMo/maxresdefault.webp)](https://www.youtube.com/watch?v=KxwkkXk_wMo "Bad Apple in Northstar server browser")

## Usage

Have Rust and Cargo installed.

Edit `json-files/auth_with_self-response.json` to set `id` to your Origin UID.

```json
{
    "success":true,
    "id":"SET-YOUR-UID-HERE", <--- Set this value to your Origin UID
    "authToken":"42",
    "persistentData": [...]
}
```

Compile and start the masterserver with `cargo run`.

## Development

Use the following command to automatically recompile and restart masterserver on file change:

```
cargo watch --quiet --clear --watch src/ --exec run
```
