mod api;
use api::api_filter;
use tokio;

#[tokio::main]
async fn main() {
    // Logging
    // Set environt variable `RUST_LOG` to `trace` or other value to enable.
    // i.e. `export RUST_LOG=trace`
    let filter = std::env::var("RUST_LOG")
        .unwrap_or_else(|_| "northstar_master_server=info,warp=debug".to_owned());
    tracing_subscriber::fmt()
        .with_env_filter(filter)
        .with_span_events(tracing_subscriber::fmt::format::FmtSpan::CLOSE)
        .init();

    let routes = api_filter();

    println!("Starting web server");
    warp::serve(routes).run(([0, 0, 0, 0], 8080)).await;
}
