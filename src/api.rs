use serde_json::json;
use warp::{reply::Json, Filter};

use std::fs;

use std::sync::atomic::AtomicU64;
use std::time::{SystemTime, UNIX_EPOCH};

static mut START_TIME: AtomicU64 = AtomicU64::new(0);

// Unimplemented endpoints:
// ========================
// This master server only has basic functionality to get to displaying server browser
// As such the following endpoints are not implemented
// - /server/add_server
// - /server/update_values
// - /server/remove_server
// - /client/servers
// - /client/auth_with_server
// - /accounts/write_persistence
// - /verify
// - /authenticate_incoming_player

pub fn api_filter() -> impl Filter<Extract = impl warp::Reply, Error = warp::Rejection> + Clone {
    let client_base = warp::path("client");

    // /client/origin_auth
    let origin_auth = client_base
        .and(warp::path("origin_auth"))
        .and_then(origin_auth_response);

    // /client/servers
    let servers = client_base
        .and(warp::path("servers"))
        .and_then(servers_response);

    // /client/mainmenupromos
    let mainmenupromos = client_base
        .and(warp::path("mainmenupromos"))
        .and(warp::fs::file("json-files/mainmenupromos.json"));

    // /client/auth_with_self
    let auth_with_self = client_base
        .and(warp::path("auth_with_self"))
        .and(warp::fs::file("json-files/auth_with_self-response.json"));

    // All routes combined
    origin_auth
        .or(servers)
        .or(mainmenupromos)
        .or(auth_with_self)
}

async fn servers_response() -> Result<Json, warp::Rejection> {
    // let mut static counter = 0;

    // Grab current time
    let current_time = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .unwrap()
        .as_millis();

    // Grab globally saved start time
    let mut start_time;
    unsafe {
        start_time = START_TIME.load(std::sync::atomic::Ordering::Relaxed);
    }

    // If start time is zero, it means we call the server browser for the first time
    // As such we set the start time to current time
    if start_time == 0 {
        let time_in_millis_u64: u64 = u64::try_from(current_time).unwrap();
        unsafe {
            START_TIME.store(time_in_millis_u64, std::sync::atomic::Ordering::Relaxed);
        }
        start_time = time_in_millis_u64;
    }

    let start_time: u128 = u128::try_from(start_time).unwrap();

    let frame_rate = 10;

    // Get current frame
    let current_frame = ((current_time - start_time) * frame_rate) / 1000;
    // println!("{}", current_frame);

    let frames = 2190;

    // Get server browser entry filename based on frame
    let data = fs::read_to_string(format!(
        "{}/{}.json",
        "json-files/frames",
        current_frame % frames
    ))
    .expect("Unable to read file");

    // Load JSON file
    let json: serde_json::Value =
        serde_json::from_str(&data).expect("JSON does not have correct format.");

    // serder-json to warp-reply
    let response = warp::reply::json(&json);

    Ok(response)
}

async fn origin_auth_response() -> Result<Json, warp::Rejection> {
    let response = json!(
        {"success":true, "token":"42"}
    );

    // serder-json to warp-reply
    let response = warp::reply::json(&response);

    Ok(response)
}
